Sessions database
=================

.. toctree::
   :maxdepth: 1

   changesessionbackend
   filesessionbackend
   sqlsessionbackend
   ldapsessionbackend
   redissessionbackend
   mongodbsessionbackend
   cassandrasessionbackend
   browseablesessionbackend
   restsessionbackend
   soapsessionbackend
